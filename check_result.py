# The purpose of this script is to assert that the mean accuracy on the given test data falls within a certain threshold.
# The build should fail if the accuracy is too low. 
import sys

if len(sys.argv) != 2:
    print("Please specify the minimum threshold for an acceptable result, like so: pipenv run python check_result.py 0.7")
    sys.exit(-1)

# Minimum bound for an acceptable result passed as a command line argument
result_threshold = float(sys. argv[1])

with open('result.txt', 'r') as f:
    result = float(f.read())

if result >= result_threshold:
    print('The model has a high enough score {} >= {}.'.format(result, result_threshold))
else:
    print('The model does not have a high enough score {} < {}.'.format(result, result_threshold))
    sys.exit(-1)