# This Python script fetches a pre-built model from an Amazon S3 bucket and runs a prediction on some sample data.
# This script is used by the CI/CD pipeline to assert that a model fetched from S3 can be downloaded and used. 
from   io import BytesIO
import boto3
import joblib
import sys

if len(sys.argv) != 3:
    print("Please specify the AWS bucket as well as the path within the bucket where the model is stored.")
    print("For example: pipenv run python smoke_test.py xxxxxxxxx-data-studio-3-models latest/pima_model.joblib")
    sys.exit(-1)

s3 = boto3.resource('s3')

aws_bucket = sys.argv[1]
model_path = sys.argv[2]

sample = [
    [ 3, 102, 44, 20, 94, 30.8, 0.4, 26 ],
    [ 9, 140, 94, 0, 0,	32.7, 0.734, 45 ]
]

with BytesIO() as data:
    s3.Bucket(aws_bucket).download_fileobj(model_path, data)
    data.seek(0) 
    loaded_model = joblib.load(data)
    print(loaded_model.predict(sample))