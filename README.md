# MLOps pipeline

## Overview

This project builds a simple MLOps CI / CD pipeline using AWS, Terraform and Gitlab.

It allows data scientists to train a model to analyse the Pima Indians Diabetes dataset and publish the trained model to AWS S3 so it can be reused for prediction. 

The pipeline can build models on git branches (main or otherwise) via the following steps: 

* infrastructure is provisioned
* model is trained and built
* verify accuracy
* upload the model to S3
* smoke test that an uploaded model can be used to make predictions

When changes are merged to the main branch (for example after code review of a **merge request**), the pipeline runs the steps above and in addition allows a user to manually promote a given model to production (marking it as the **latest** version to be used).

## Requirements

You will need the following tools installed locally: 

* Python
* pipenv (https://pipenv.pypa.io/en/latest/)
* tfenv (https://github.com/tfutils/tfenv) or Terraform (version 1.2.2) (if you want to provision the infrastructure locally)
* aws cli (https://aws.amazon.com/cli/). 

You will need a gitlab account (the free version from https://gitlab.com/ is sufficient).

You will need an AWS account. It is recommended to create a separate IAM user (for example CI_USER) with rights to be able to push to Amazon S3 (AmazonS3FullAccess). Then follow the instructions from GitLab to configure the pipeline with the AWS credentials of the IAM user: https://docs.gitlab.com/ee/ci/cloud_deployment/

On your local machine, run `aws configure` and provide the credentials of the account of your choice (either yours or the IAM User with S3 permissions). The credentials are also used by Terraform locally to setup the rest of the S3 infrastructure (should you wish to do so).

## Pipenv

I've setup the project to use pipenv in order to make more predictable builds and manage the Python environment. 

The python scripts in the project can be run like so `pipenv run python a_script.py`.

## Terraform infrastructure

The project uses Terraform to create the S3 bucket used to store the models. 

### Terraform state bucket

Terraform uses an S3 bucket (currently xxxxxxxxx-data-studio-3-terraform) to store its state. That S3 bucket has to be created manually prior to Terraform running. The state bucket can be created via the aws cli or via the AWS console.

For example: 
```shell
aws s3api create-bucket --bucket xxxxxxxxx-data-studio-3-terraform --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1
```

Alternatively see the script `create_terraform_state_bucket.sh`.

### Models bucket

You can run the script `create_s3_bucket.sh` locally to create the AWS S3 bucket to store the models.

The pipeline will also run the same commands to ensure the S3 bucket exists with the correct configuration. Basically, it will do the following:  

```shell
cd terraform
terraform init
terraform apply --auto-approve
```
The models S3 bucket allows read-only access for anonymous requests. 

## GitLab pipeline. 

To run the pipeline, create a Git repo in GitLab (for example data-studio-3) and upload this code via a git push. When you push the code (to `main` or to any branch), the GitLab pipeline will run the various stages of the ML Ops pipeline. 

As the pipeline runs, it will push a trained model to the S3 bucket using the GitLab pipeline ID for the given build, or **latest** for the most recently promoted build. 

## Testing pre-trained models
You can smoke test any models that have been previously built by running the following locally: 

```shell
pipenv run python smoke_test.py xxxxxxxxx-data-studio-3-models 563857533/pima_model.joblib
```

Where 563857533 is the id of a previous GitLab pipeline. The smoke test ensures that a trained model does not throw any errors when trying to classify sample data. 

You can also do

```shell
pipenv run python smoke_test.py xxxxxxxxx-data-studio-3-models latest/pima_model.joblib
```

to smoke test the latest model which was promoted to prod.