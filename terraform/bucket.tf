resource "aws_s3_bucket" "bucket" {
  bucket = "xxxxxxxxx-data-studio-3-models"
}

resource "aws_s3_bucket_policy" "bucket_policy" {  
  bucket = aws_s3_bucket.bucket.id   
policy = <<POLICY
{    
    "Version": "2012-10-17",    
    "Statement": [        
      {            
          "Sid": "PublicReadGetObject",            
          "Effect": "Allow",            
          "Principal": "*",            
          "Action": [                
             "s3:GetObject",
             "s3:GetObjectVersion"            
          ],            
          "Resource": [
             "arn:aws:s3:::${aws_s3_bucket.bucket.id}/*"            
          ]        
      }    
    ]
}
POLICY
}