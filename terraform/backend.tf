terraform {
  backend "s3" {
    bucket = "xxxxxxxxx-data-studio-3-terraform"
    key    = "tf_state"
    region = "eu-west-1"
  }
}